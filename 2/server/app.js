let express = require('express')

let app = express()

require('./app/setup')(app)

module.exports = app