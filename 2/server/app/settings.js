let settings = {
	port: 8888,
	url: () => `http://localhost:${settings.port}`
}

module.exports = settings