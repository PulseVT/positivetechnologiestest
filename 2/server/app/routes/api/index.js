let router = require('express').Router()

module.exports = app => {

	router.use('/books', require('./books.route')(app))
	router.use('/authors', require('./authors.route')(app))

	return router
}