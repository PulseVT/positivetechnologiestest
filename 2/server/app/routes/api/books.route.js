let router = require('express').Router()
let _ = require('lodash')

module.exports = app => {

	router.get('/', (req, res) => {
		res.send({
			results: app.data.books
		})
	})

	router.get('/:id', (req, res, next) => {
		let book = _.find(app.data.books, {id: parseInt(req.params.id)})

		if(book!=null){
			res.send(book)
		} else {
			res.status(404).send({error: 'Book not found'})
		}
	})

	router.post('/', (req, res) => {
		let now = Date.now()
		let book = {
			id: now,
			name: req.body.name,
			author: req.body.author,
			text: req.body.text,
			rating: req.body.rating,
			year: req.body.year,
			date: now,
			img: app.data.randomImg()
		}

		app.data.books.push(book)
		let author = _.find(app.data.authors, {id: book.author})
		author.books.push(book.id)

		console.log(`Book added: ${JSON.stringify(book)}`)

		res.send(book)
	})

	return router

}