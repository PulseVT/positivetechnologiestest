let router = require('express').Router()
let _ = require('lodash')

module.exports = app => {

	router.get('/', (req, res) => {
		res.send({
			results: app.data.authors
		})

	})

	router.get('/:id', (req, res, next) => {
		let author = _.find(app.data.authors, {id: parseInt(req.params.id)})

		if(author!=null){
			res.send(author)
		} else {
			res.status(404).send({error: 'Author not found'})
		}
	})

	return router

}