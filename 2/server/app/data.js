let _ = require('lodash')
let settings = require('./settings')
let random = (n) => Math.round(Math.random()*n)
let lorems = [
	"Ipsum leo ipsum ligula lectus ipsum mattis morbi risus nulla eros massa risus. Diam ipsum vitae nulla ipsum at massa auctor pellentesque vitae auctor eu enim. Malesuada risus vulputate gravida donec orci vivamus non nibh magna. Urna massa magna sed pharetra curabitur elementum quam congue morbi sed ornare.",
	"Commodo odio sodales lorem ut ornare in arcu risus elementum quisque rutrum at rutrum adipiscing vitae sagittis commodo gravida ultricies gravida massa mauris donec. Sapien urna nulla eros eget amet porttitor donec curabitur sit nulla lorem curabitur commodo sapien metus arcu fusce lectus vivamus tempus. Risus commodo molestie non justo donec tempus orci mattis orci massa tempus ornare duis diam sapien tellus nec odio quam. Leo amet fusce commodo sit commodo massa odio sed bibendum sed eu ornare a justo vivamus eros adipiscing et porttitor massa. Eu at orci sit eu pellentesque pharetra eu quam pellentesque sapien proin fusce sed a magna bibendum porttitor.",
	"Quisque metus porta cursus morbi sodales vivamus bibendum fusce elementum mauris elementum mauris. Sit integer malesuada at nec justo tellus ornare sed elementum cursus pellentesque adipiscing vulputate magna non ipsum mattis lectus. Risus rutrum a et molestie ornare lorem sodales sagittis adipiscing sem mauris donec ut cursus sodales sit risus tellus quam.",
	"Rutrum magna integer adipiscing risus magna orci et lectus non leo duis malesuada metus auctor lectus sed. Pharetra eu gravida donec molestie non nam donec maecenas proin tempus nam porttitor amet quisque ornare eros nec vitae urna eu eros cursus. Sapien leo bibendum magna lorem molestie lectus orci fusce gravida arcu ornare massa arcu quisque integer.",
	"Et lectus rutrum mauris rutrum urna nibh at cursus orci duis urna eu justo fusce porta fusce ornare leo ornare pharetra vitae eget congue. Tellus ultricies et nibh tempus pharetra pellentesque arcu orci ut in metus ornare bibendum mattis massa diam orci sagittis risus. Arcu adipiscing pharetra eget odio gravida sit orci proin tempus rutrum porta leo elementum morbi."
]
let imgs = [
	'qwe.png',
	'angular.jpg',
	'node.png'
]

const nBOOKS = 100, nAUTHORS = 25

let data = {
	books: [],
	authors: [],
	randomImg () {
		return `${settings.url()}/${imgs[random(imgs.length-1)]}`
	}
}

for(let i=0; i<nAUTHORS; i++) {
	data.authors.push({
		id: i,
		name: `Author_${i}`,
		books: []
	})
}

let now = new Date
for(let i=0; i<nBOOKS; i++){
	let author = _.sample(data.authors)
	let date = new Date
	date.setDate(date.getDate() - random(1000))
	data.books.push({
		id: i,
		name: `Book_${i}`,
		author: author.id,
		text: `${i} _ ${lorems[random(lorems.length-1)]}`,
		rating: random(5),
		year: random(2016),
		date: date.valueOf(),
		image: data.randomImg()
	})
	author.books.push(i)
}

module.exports = app => app.data = data