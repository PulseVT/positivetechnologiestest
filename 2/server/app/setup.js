let bodyParser = require('body-parser')
let settings = require('./settings')
let cors = require('cors')
let express = require('express')

module.exports = app => {

	app.use(bodyParser.json())
	app.use(cors())
	app.use(express.static('img'))

	require('./data')(app)
	require('./routes')(app)

	app.listen(settings.port, () => {
		console.log(`Started server on ${settings.port} port`)
	})

}