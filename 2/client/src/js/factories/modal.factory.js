{
	let module = angular.module('PTTest.factories.modal', [])

	module.factory('Modal', [
		'Class',
		'$uibModal',
		(Class, $uibModal) =>
			class Modal extends Class {
				open (context) {
					let options = {
						resolve: {
							context: () => context,
							instance: () => this
						},
						animation: true,
						controller: this.controller,
						controllerAs: this.controllerAs || '$ctrl',
						templateUrl: this.templateUrl
					}

					this.$instance = $uibModal.open(options)

					return this.$instance
				}
			}
	])
}