{
	let module = angular.module('PTTest.factories.class', [])

	module.factory('Class', [
		() =>
			class Class {
				constructor (properties) {
					this.set(properties)
				}

				set (properties) {
					_.extend(this, properties)
				}
			}
	])
}