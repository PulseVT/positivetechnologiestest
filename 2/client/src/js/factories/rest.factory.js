{
	let module = angular.module('PTTest.factories.rest', [])

	module.factory('RestFactory', [
		'Collection',
		'Restangular',
		(Collection, Restangular) =>
			class RestFactory extends Collection {
				constructor (rest='', config={}) {
					_.defaults(config, {
						id_field: 'id'
					})

					if(_.isString(rest))
						rest = Restangular.one(rest)

					super(rest, config)
				}
			}
	])
}