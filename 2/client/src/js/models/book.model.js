{
	let module = angular.module('PTTest.models.book', [])

	module.factory('Book', [
		'Class',
		'authors',
		(Class, authors) =>
			class Book extends Class {
				constructor (properties) {
					super(properties)

					authors.fetch({id: this.author}).then(() => this._authorName = this.authorName)
				}

				get authorName() {
					let author = authors.by(this.author), authorName=''
					if(author!=null)
						authorName = author.name
					return authorName
				}

				get authorId() {
					let author = authors.by(this.author)
					if(author!=null)
						return author.id
				}

				get shortText() {
					return this.text.substr(0, 100)
				}

				get dateFormatted() {
					let date = new Date(this.date)
					return date.toLocaleDateString()
				}
			}
	])
}