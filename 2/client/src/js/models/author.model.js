{
	let module = angular.module('PTTest.models.author', [])

	module.factory('Author', [
		'Class',
		(Class) =>
			class Author extends Class {
				constructor (properties) {
					super(properties)
				}

				get booksNumber() {
					return this.books.length
				}
			}
	])
}