{
	let module = angular.module('PTTest.pages.search', [])

	module.config([
		'$stateProvider',
		$stateProvider => {
			$stateProvider.state('search', {
				url: '/search',
				views: {
					main: {
						controller: 'SearchPageCtrl',
						controllerAs: '$ctrl',
						templateUrl: 'js/pages/search/search.page.html'
					}
				},
				data: {
					name: 'Search',
					inHeader: true
				}
			})
		}
	])

	module.controller('SearchPageCtrl', [
		'books',
		'authors',
		'bookModal',
		function(books, authors, bookModal){
			books.fetch()
			authors.fetch()

			_.extend(this, {
				books,
				filter: {},

				fields: [
					{
						name: 'name',
						title: 'Name',
						sortable: true,
						filterable: true
					},
					{
						name: 'rating',
						title: 'Rating',
						sortable: true,
						filterable: true
					},
					{
						name: 'authorName',
						title: 'Author',
						sortable: true,
						filterable: true
					},
					{
						name: 'year',
						title: 'Year',
						sortable: true,
						filterable: true
					},
					{
						name: 'text',
						title: 'Text',
						filterable: true
					}
				],

				bookClicked (book) {
					bookModal.setDetailsMode()
					bookModal.open({
						book
					})
				}
			})
		}
	])
}