{
	let module = angular.module('PTTest.pages.author', [])

	module.config([
		'$stateProvider',
		$stateProvider => {
			$stateProvider.state('author', {
				url: '/author/:id',
				views: {
					main: {
						controller: 'AuthorPageCtrl',
						controllerAs: '$ctrl',
						templateUrl: 'js/pages/author/author.page.html'
					}
				},
				resolve: {
					author: [
						'authors',
						'$stateParams',
						'router',
						'$timeout',
						(authors, $stateParams, router, $timeout) => {
							let redirect = () =>
								$timeout(() =>
									router.go('home', {}, {location:'replace'})
								)
							if(!$stateParams.id)
								redirect()
							else {
								let data = {
									id: $stateParams.id
								}
								return authors.fetch(data).catch(redirect)
							}
						}
					]
				}
			})
		}
	])

	module.controller('AuthorPageCtrl', [
		'author',
		'books',
		function(author, books){
			_.extend(this, {
				author,
				books,

				getBooksArr () {
					return _.compact(author.books.map((bookId) => books.by(bookId)))
				}
			})

			for(let i=0;i<author.books.length;i++){
				books.fetch({id: author.books[i]})
			}
		}
	])
}