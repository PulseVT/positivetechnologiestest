{
	let module = angular.module('PTTest.pages.book', [])

	module.config([
		'$stateProvider',
		$stateProvider => {
			$stateProvider.state('book', {
				url: '/book/:id',
				views: {
					main: {
						controller: 'BookPageCtrl',
						controllerAs: '$ctrl',
						templateUrl: 'js/pages/book/book.page.html'
					}
				},
				resolve: {
					book: [
						'books',
						'$stateParams',
						'$state',
						'$timeout',
						(books, $stateParams, $state, $timeout) => {
							let redirect = () =>
								$timeout(() =>
									router.go('search', {}, {location:'replace'})
								)
							if(!$stateParams.id)
								redirect()
							else {
								let data = {
									id: $stateParams.id
								}
								return books.fetch(data).catch(redirect)
							}
						}
					]
				}
			})
		}
	])

	module.controller('BookPageCtrl', [
		'book',
		function(book){
			_.extend(this, {
				book
			})
		}
	])
}