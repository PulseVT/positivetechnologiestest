{
	let module = angular.module('PTTest.pages.home', [])

	module.config([
		'$stateProvider',
		$stateProvider => {
			$stateProvider.state('home', {
				url: '/',
				views: {
					main: {
						controller: 'HomePageCtrl',
						controllerAs: '$ctrl',
						templateUrl: 'js/pages/home/home.page.html'
					}
				},
				data: {
					name: 'Home',
					inHeader: true
				}
			})
		}
	])

	module.controller('HomePageCtrl', [
		'books',
		'authors',
		function(books, authors){
			books.fetch()
			authors.fetch()

			_.extend(this, {
				books,
				authors
			})

		}
	])
}