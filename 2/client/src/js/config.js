{
	let module = angular.module('PTTest.config', [])

	module.config([
		'$locationProvider',
		'RestangularProvider',
		'API_URL',
		'$qProvider',
		($locationProvider, RestangularProvider, API_URL, $qProvider) => {
			$locationProvider.html5Mode(true)

			RestangularProvider.setBaseUrl(API_URL)

			$qProvider.errorOnUnhandledRejections(false)
		}
	])
}