{
	let module = angular.module('PTTest.router', [])

	module.config([
		'$urlRouterProvider',
		$urlRouterProvider => {
			$urlRouterProvider.otherwise('/')
		}
	])

	module.service('router', [
		'$state',
		$state => (
			{
				go (stateName, params={}, options={}) {
					_.defaultsDeep(options, {
						inherit: true
					})
					$state.go(stateName, params, options)
				}
			}
		)
	])
}