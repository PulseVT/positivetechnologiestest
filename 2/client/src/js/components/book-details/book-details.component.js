{
	let module = angular.module('PTTest.components.book-details', [])

	module.component('bookDetails', {
		templateUrl: 'js/components/book-details/book-details.component.html',
		bindings: {
			book: '='
		}
	})
}