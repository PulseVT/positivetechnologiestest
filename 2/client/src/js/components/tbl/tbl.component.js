{
	let module = angular.module('PTTest.components.tbl', [])

	module.component('tbl', {
		controller: 'TblCtrl',
		templateUrl: 'js/components/tbl/tbl.component.html',
		bindings: {
			items: '=',
			fields: '<',
			pageSize: '<',
			disable: '&',
			itemClicked: '='
		}
	})


	module.controller('TblCtrl', [
		'$scope',
		'$filter',
		'$rootScope',
		function($scope, $filter, $rootScope) {
			const defaults = {
				pageSize: 25
			}

			let ctrl = this

			_.extend(this, {
				filters: {},

				initPaging (){
					_.extend(ctrl, {
						page: 0,
						arr: []
					})
					ctrl.sourceItems = $filter('filter')(ctrl.items, ctrl.filters.common)
					ctrl.sourceItems = $filter('filter')(ctrl.sourceItems, ctrl.filters.fields)
					if(ctrl.sortField!=null)
						ctrl.sourceItems = $filter('orderBy')(ctrl.sourceItems, ctrl.sortField.name, ctrl.sortField.sortDirection)
					ctrl.getMore()
				},

				getMore () {
					let pageSize = ctrl.pageSize || defaults.pageSize
					let start = ctrl.page*pageSize
					ctrl.arr = ctrl.arr.concat(ctrl.sourceItems.slice(start, start+pageSize))
					ctrl.page++
				},

				sort (field) {
					if(ctrl.sortField === field){
						field.sortDirection = !field.sortDirection
					} else {
						field.sortDirection = false
						ctrl.sortField = field
					}
					ctrl.initPaging()
				}
			})

			$scope.$watch(() => {
				if(this.items!=null)
					return this.items.length
			}, value => {
				if(value) {
					this.initPaging()
				}
			})

			$scope.$watch(() => {
				return this.filters
			}, filters => {
				if(filters!=null)
					this.initPaging()
			}, true)
		}
	])
}