{
	let module = angular.module('PTTest.components.tbl-filter', [])

	module.component('tblFilter', {
		templateUrl: 'js/components/tbl-filter/tbl-filter.component.html',
		bindings: {
			model: '=',
			title: '@'
		}
	})
}