{
	let module = angular.module('PTTest.components.main', [])

	module.component('main', {
		templateUrl: 'js/components/main/main.component.html',
		controller: 'MainComponentCtrl'
	})

	module.controller('MainComponentCtrl', [
		function(){
		}
	])
}