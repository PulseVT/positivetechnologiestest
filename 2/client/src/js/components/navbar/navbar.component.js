{
	let module = angular.module('PTTest.components.navbar', [])

	module.component('navbar', {
		templateUrl: 'js/components/navbar/navbar.component.html',
		controller: 'NavbarComponentCtrl'
	})

	module.controller('NavbarComponentCtrl', [
		'$state',
		'bookModal',
		function($state, bookModal){
			_.extend(this, {
				$state,

				getAllStates() {
					return $state.get().filter(state => state.data!=null && state.data.inHeader && !state.abstract)
				}, 

				addBook() {
					bookModal.setAddMode()
					bookModal.open()
				}
			})
		}
	])
}