{
	let module = angular.module('PTTest.rest.authors', [])

	module.service('authors', [
		'RestFactory',
		'Author',
		(RestFactory, Author) =>
			new RestFactory('authors', {
				model: Author
			})
	])
}