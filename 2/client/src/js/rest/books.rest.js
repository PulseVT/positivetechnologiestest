{
	let module = angular.module('PTTest.rest.books', [])

	module.service('books', [
		'RestFactory',
		'Book',
		(RestFactory, Book) =>
			new RestFactory('books', {
				model: Book
			})
	])
}