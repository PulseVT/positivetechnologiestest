{
	let module = angular.module('PTTest.modals.book', [])

	module.factory('bookModal', [
		'Modal',
		Modal => {
			class BookModal extends Modal {
				constructor() {
					super()

					this.set({
						controller: 'BookModalCtrl',
						templateUrl: 'js/modals/book/book.modal.html'
					})

					this.setDetailsMode()
				}

				setAddMode () { this.addMode = true }
				setDetailsMode () { this.addMode = false }
			}

			return new BookModal
		}
	])

	module.controller('BookModalCtrl', [
		'context',
		'instance',
		'$state',
		'$scope',
		'authors',
		'books',
		function(context, instance, $state, $scope, authors, books){
			authors.fetch()

			let ctrl = this
			_.extend(this, {
				authors,
				context,
				instance,
				newBook: {},

				dismiss () {
					instance.$instance.dismiss()
				},

				goToPage() {
					ctrl.dismiss()
					$state.go('book', {id: context.book.id})
				},

				add() {
					books.create(ctrl.newBook).then(book => {
						authors.by(book.author).books.push(book.id)
						ctrl.dismiss()
					})
				}
			})

			$scope.$on('$stateChangeSuccess', () => this.dismiss())
		}
	])
}