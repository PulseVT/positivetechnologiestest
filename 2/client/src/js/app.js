angular.module('PTTest', [
	'ui.bootstrap',
	'ui.router',
	'ng-bundle-collection',
	'restangular',
	'infinite-scroll',

	'PTTest.constants',
	'PTTest.config',
	'PTTest.router',
	'PTTest.filters',

	'PTTest.factories.class',
	'PTTest.factories.rest',
	'PTTest.factories.modal',

	'PTTest.modals.book',

	'PTTest.models.book',
	'PTTest.models.author',

	'PTTest.rest.books',
	'PTTest.rest.authors',

	'PTTest.components.main',
	'PTTest.components.navbar',
	'PTTest.components.tbl',
	'PTTest.components.tbl-filter',
	'PTTest.components.book-details',

	'PTTest.pages.home',
	'PTTest.pages.search',
	'PTTest.pages.book',
	'PTTest.pages.author'
])