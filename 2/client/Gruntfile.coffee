module.exports = (grunt) ->

	require('load-grunt-tasks') grunt
	modrewrite = require 'connect-modrewrite'
	serveStatic = require 'serve-static'

	grunt.initConfig
	
		connect:
			client:
				options:
					port: 9999
					middleware: (connect, options) ->
						[
							modrewrite [ '!\\.html|\\.js|\\.css|\\.woff|\\.woff2|\\.ttf$ /index.html [L]' ]
							serveStatic 'build'
							serveStatic '.'
						]

		watch:
			js:
				files: [
					'src/**/*.js'
				]
				tasks: ['babel:client']
			less:
				files: [
					'src/**/*.less'
				]
				tasks: ['less:client']
			index:
				files: [
					'index.template.html'
					'bower.json'
				]
				tasks: [
					'includeSource:client'
					'wiredep:client'
				]
			html:
				files: [
					'src/**/*.html'
				]
				tasks: [
					'ngtemplates:client'
					'includeSource:client'
					'wiredep:client'
				]

		babel:
			options:
				presets: [
					'es2015'
				]
			client:
				expand: yes
				cwd: 'src'
				src: [
					'**/*.js'
				]
				dest: 'build'

		less:
			client:
				files:
					'build/styles.css': 'src/less/app.less'


		includeSource:
			client:
				options:
					basePath: "#{__dirname}/build"
				files:
					'build/index.html': 'index.template.html'

		wiredep:
			client:
				src: [
					'build/index.html'
				]
				options:
					overrides:
						bootstrap:
							main: [
								'dist/js/bootstrap.min.js'
								'dist/css/bootstrap.min.css'
							]

		ngtemplates:
			client:
				options:
					module: 'PTTest'
				cwd: 'src'
				src: '**/*.html'
				dest: 'build/templates.js'

		clean:
			build: [
				'build'
			]

	grunt.registerTask 'build', [
		'less:client'
		'babel:client'
		'ngtemplates:client'
		'includeSource:client'
		'wiredep:client'
	]

	grunt.registerTask 'serve', [
		'clean:build'
		'build'
		'connect:client'
		'watch'
	]

	grunt.registerTask 'default', [
		'serve'
	]