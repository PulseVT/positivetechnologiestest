const selectedCart = [
   { price: 20 },
   { price: 45 },
   { price: 67 },
   { price: 1305 }
];

//dollar to
const rates = {
	ruble: 60.87,
	euro: 0.94,
	dollar: 1,
	pound: 0.7938,
	yen: 86.71
}

const sum = selectedCart.reduce((sum, item) => sum + item.price, 0)

let totalCartPrice = {}

for(let k in rates){
	totalCartPrice[k] = sum * rates[k]
}

alert(JSON.stringify(totalCartPrice))